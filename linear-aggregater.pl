#!/usr/bin/perl
use GreenLogger;
use strict;
use autodie qw(open close);
use Data::Dumper;
use JSON::XS;

# This script reads the stats from other files
# and it generates an aggregate.csv and aggregate.json
# but it does so linearly without using a lot of memory
# This performs better than the other aggregator and uses less memory
# It relies on a callback mechanism

# To use it call
# cat filenames-of-greenlog.txt.gz | perl linear-aggregator.pl
# or
# perl linear-aggregator.pl *greenlog.txt.gz

# the -nojson flag avoids writing json to files

# check for json arguments the bad way
my $data;
my $JSON = 1;
if (@ARGV && $ARGV[0] eq "-nojson") {
	$JSON = 1;
	shift @ARGV;
}

# Now determine how we get a list of files
my @files = ();
if (@ARGV) {
    @files = @ARGV;
} else {
    my @elms = <STDIN>;
    chomp(@elms);
    @files = @elms;
}

# headers
my @expNames = qw(
    machine   
    utime     
    sURI      
    path      
    file      
    date      
    ffversion 
);

# indicate if we're at the first element or not
my $first = 1;

# header names to updated once we read the file
my @wattNames = ();

#open the files
open(my $fd,">","aggregate.csv");
my $jsonfd;
if ($JSON) {
    open($jsonfd,">","aggregate.json");
    print $jsonfd "[$/";
}
#json first
my $jsonfirst = 1;

# our main callback routine
my $callback = sub {
    my ($elm) = @_;

    # add a wattage summary stats
    $elm->{wattage} = GreenLogger::wattageSummary($elm);
    
    # first element we print a header
    if ($first) {
	@wattNames = sort keys %{$elm->{wattage}};
        my $str = join(",",@expNames,@wattNames).$/;
        print $fd $str;
        print $str;
	$first = 0;
    }

    #now deal with the CSV
    my $str = join(",", 
               (map { my $x = $_; 
                      my $v = $elm->{$x};
                      ($v=~/^\d+$/)?($v):("\"$v\"")
                  } @expNames),
               (map { my $x = $_;
                      $elm->{wattage}->{$x}
                  } @wattNames)).$/;
    print $fd $str;
    print $str;

    # on json
    if ($JSON) {

        #jsons sucks
	print $jsonfd ",$/" unless $jsonfirst;
	print $jsonfd encode_json($elm);
	print $jsonfd $/;
	$jsonfirst = 0;

    }

};

#throw our callback into our files
$data = GreenLogger::forEachFile($callback,  @files );

# close everything
close($fd);
warn "Done with aggregate.csv";
if ($JSON) {
    print $jsonfd "$/]$/";
    close($jsonfd);
    warn "Done with aggregate.json";
}
