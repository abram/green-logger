#!/usr/bin/perl
use GreenLogger;
use strict;
use autodie qw(open close);
use Data::Dumper;
use JSON::XS;
my $data;
my $JSON = 1;
if (@ARGV && $ARGV[0] eq "-nojson") {
	$JSON = 0;
	shift @ARGV;
}
if (@ARGV) {
    $data = GreenLogger::getAllData( @ARGV );
} else {
    my @elms = <STDIN>;
    chomp(@elms);
    $data = GreenLogger::getAllData( @elms );
}
my @data = map { my $elm = $_; $elm->{wattage} = GreenLogger::wattageSummary($elm); $elm } @$data;

my @expNames = qw(
    machine   
    utime     
    sURI      
    path      
    file      
    date      
    ffversion 
);
my @wattNames = sort keys %{$data[0]->{wattage}};

open(my $fd,">","aggregate.csv");               
my $str = join(",",@expNames,@wattNames).$/;
print $fd $str;
print $str;
foreach my $elm (@data) {
    warn $elm;
    my $str = join(",", 
               (map { my $x = $_; 
                      my $v = $elm->{$x};
                      ($v=~/^\d+$/)?($v):("\"$v\"")
                  } @expNames),
               (map { my $x = $_;
                      $elm->{wattage}->{$x}
                  } @wattNames)).$/;
    print $fd $str;
    print $str;
}
close($fd);
warn "Done with aggregate.csv";
exit(0) unless $JSON;
#open(my $fd,">","aggregate.dump");               
#print $fd Dumper(\@data);
#close($fd);
open(my $fd,">","aggregate.json");
print $fd "[$/";
my $first = 1;
foreach my $elm (@data) {
	#print $fd Dumper(\@data);
	print $fd ",$/" unless $first;
	print $fd encode_json($elm);
	print $fd $/;
	$first = 0;
}
print $fd "$/]$/";
close($fd);

warn "Done with aggregate.json";
