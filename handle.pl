use strict;
use IO::Handle;
my $handle = IO::Handle->new_from_fd( *STDIN, "r");
$handle->blocking( 0 );
$handle->autoflush( 1 );
my $cnt = 0;
while( (my $a = $handle->getline()) || 1) {
	print "$cnt [$a]" if $a;
	$cnt++;
}
