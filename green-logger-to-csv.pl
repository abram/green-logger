#!/usr/bin/perl
use strict;
open(my $fd,$ARGV[0] || "-");
my @lines = <$fd>;
close($fd);
chomp(@lines);
my $code = join($/,@lines);
my @OUT;
my $VAR1;
eval $code;
my %h = ();
# now get the headers
foreach my $elm (@OUT) {
    my @headers = addHeaders([],undef,$elm);
    foreach my $header (@headers) {
        unless (exists $h{$header->[0]}) {
            $h{$header->[0]} = $header->[1];
        }
    }
}
my @keys = sort keys %h;
print join(",", @keys),$/;
foreach my $elm (@OUT) {
    print join(",", map { access($elm, $h{$_}) } @keys),$/;
}
sub access {
    my ($elm, $arr) = @_;
    my $e = $elm;
    my $e = $elm;
    eval {
    foreach my $key (@$arr) {  
        $e = $e->{$key};
    }
    };
    return undef if $@;
    return $e;
}

sub addHeaders{
    my ($prefix,$key,$val) = @_;
    my @karr = @$prefix;
    push @karr, $key if defined($key);
    if (ref($val)) {
        return map { 
            my $key2 = $_; 
            my $val2 = $val->{$key2};
            addHeaders([@karr],$key2,$val2);
        } keys %$val;
    } else {
        my $name = join(".",@karr);
        if ($name =~ /[\s\/%]/) {
            $name = "\"$name\"";
        }
        return [$name, [@karr]];
    }
}
