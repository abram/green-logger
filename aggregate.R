v <- read.csv("aggregate.csv")
#v <- read.csv("aggregate.round1.csv")
#ov <- v[order(v["date"]),]
ov <- v[order(v["sURI"]),]
ov <- ov[ov$mean > 10,]

minify <- function(x) {
  m <- min(x[is.finite(x)])
  x[!is.finite(x)] <- m
}

ffsplit <- function(x) {
  y <- as.character(x)
  l <- unlist(
              strsplit(
                       gsub("pre","\\.pre",gsub("([ab])","\\.\\1\\.",y)),
                       "\\."))
  l
}

rtorrentsplit <- function(x) {
  y <- as.character(x)
  l <- unlist(
              strsplit(y,"\\-"))
                       
  l
}

getRtorrentVersion <- function(ov) {
  sapply(ov$sURI,function(s) { rtorrentsplit(s)[2] })
}

getVuzeVersion <- function(ov) {
  sapply(ov$sURI,function(s) {
    gsub("\\.jar","", rtorrentsplit(s)[2]) })
}

getRtorrentVersion <- function(ov) {
  sapply(ov$sURI,function(s) { rtorrentsplit(s)[2] })
}


getLtorrentVersion <- function(ov) {
  sapply(ov$sURI,function(s) { rtorrentsplit(s)[4] })
}


cmplabels <- c("a","b","pre","else","NA")

cmpvals  <-
  #   "a","b","pre","else","NA"
  c(  0 , -1,   -1,  -1,-1, #a
    1,   0,   -1,  -1,-1, #b
    1,   1,    0,   -1,-1, #pre
    1,   1,    1,    0, -1, #else
    1,   1,    1,    1, 0) #NA

#assume positive
ffelm2number <- function(x) {
  x <- if (is.nan(x)) { "NA" } else { x }
  x <- if (is.na(x)) { "NA" } else { x }
  x1 <- match(as.character(x),cmplabels)
  x1 <- if (is.nan(x1) | is.na(x1)) { 4 } else { x1 }
  if (x1 == 4) { #number
    x1
  } else if (x1 == 5) { #NA
    -1 * x1
  }
}

ffclt <- function(x,y) {
  x <- if (is.nan(x)) { "NA" } else { x }
  y <- if (is.nan(y)) { "NA" } else { y }
  x <- if (is.na(x)) { "NA" } else { x }
  y <- if (is.na(y)) { "NA" } else { y }
  x1 <- match(as.character(x),cmplabels)
  x1 <- if (is.nan(x1) | is.na(x1)) { 4 } else { x1 }
  y1 <- match(as.character(y),cmplabels)
  y1 <- if (is.nan(y1) | is.na(y1)) { 4 } else { y1 }
  if (x1 == 4 & y1 == 4) {
    as.numeric(x) < as.numeric(y)
  } else {
    x1 < y1
  }
}

fflt <- function(x,y) {
  xl <- ffsplit(x)
  yl <- ffsplit(y)
  len <- max(c(length(xl),length(yl)))
  for (i in 1:len) {
    if ( ffclt( xl[i] , yl[i] ) ) {
      return(TRUE)
    }
    if ( ffclt( yl[i] , xl[i] ) ) {
      return(FALSE)
    }
  }
  return(FALSE)
}

bubblesort <- function(l,mylt) {
  len <- length(l)
  for (i in 1:len) {   
    for (j in i:len) {
      v <- mylt(l[i],l[j])
      if ( v == FALSE) {
        t <- l[i]
        l[i] <- l[j]
        l[j] <- t
      }
    }
  }
  l
}

 quicksort <- function(xs, mylt) {
    if (length(xs)<=1) { xs }
    else {
            x <- xs[1]
            xs <- xs[-1]
            left  <- xs[sapply(xs,function(y){ mylt(y,x)})]
            right  <- xs[sapply(xs,function(y){ !mylt(y,x)})]
            c(quicksort(left, mylt),x,quicksort(right, mylt))
    }
 }


library(zoo)

ov <- ov[ov$mean < 28,]
ov <- ov[ov$mean > 21,]

ov <- ov[ov$seconds > 250,]
ov <- ov[as.character(ov$ffversion) != "",]
#ov <- ov[as.character(ov$ffversion) != "3.1b3pre",]
ov <- ov[as.character(ov$ffversion) != "3.6.13",]

#plot(ov$seconds)

sov <- v[v$seconds < 250,]
sov <- sov[order(sov$date),]

#plot(ov$date,ov$mean)
plot(ov$mean)
lines(rollmean(ov$mean,30,na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$mean),30,'max',na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$mean),30,'min',na.pad=TRUE,align="center"))

plot(ov$kwh)
lines(rollkwh(ov$kwh,30,na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$kwh),30,'max',na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$kwh),30,'min',na.pad=TRUE,align="center"))

plot(ov$kwh)
lines(rollkwh(ov$kwh,5,na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$kwh),5,'max',na.pad=TRUE,align="center"))
lines(rollapply(as.zoo(ov$kwh),5,'min',na.pad=TRUE,align="center"))


plot(ov$kwh)
lines(rollkwh(ov$kwh,30,na.pad=TRUE,align="left"))
lines(rollapply(as.zoo(ov$kwh),30,'max',na.pad=TRUE,align="left"))
lines(rollapply(as.zoo(ov$kwh),30,'min',na.pad=TRUE,align="left"))

 plot(ov$date,ov$mean)
lines(ov$date,rollmean(ov$mean,30,na.pad=TRUE,align="right"),col="red",lw=4)
lines(ov$date,rollapply(as.zoo(ov$mean),30,'max',na.pad=TRUE,align="right"),col="darkblue",lt=3,lw=2)
lines(ov$date,rollapply(as.zoo(ov$mean),30,'min',na.pad=TRUE,align="right"),col="darkblue",lt=3,lw=2)
#lines(ov$mean,lw=3,col="red")

plot(ov$seconds)
summary(ov$seconds)

plot(ov$ffversion, ov$mean)
plot(ov$ffversion, ov$var)
plot(ov$ffversion, ov$std)
plot(ov$ffversion, ov$max)
plot(ov$ffversion, ov$min)
fov <- ov[order(ov$ffversion),]
plot(fov$ffversion, ov$kwh)

ffversionorder <- bubblesort(as.character(unique(ov$ffversion)),fflt)
#min36 <- c(1:length(ffversionorder))[ffversionorder == "3.6a2pre"]
#ffversionorder <- ffversionorder[c(min36:length(ffversionorder))]
      l <- lapply(ffversionorder,function(x){ov[ov$ffversion == x,c("mean")]})
ffmeans <- lapply(ffversionorder,function(x){ov[ov$ffversion == x,c("mean")]})
ffkwh   <- lapply(ffversionorder,function(x){ov[ov$ffversion == x,c("kwh")]})
ffwh   <- lapply(ffversionorder,function(x){1000*ov[ov$ffversion == x,c("kwh")]})
ffvars <- lapply(ffversionorder,function(x){ov[ov$ffversion == x,c("var")]})
kwhpvs <- sapply(c(2:length(l)), function(j) { i <- j - 1; t.test(ffkwh[[i]],ffkwh[[j]])$p.value })
meanspvs <- sapply(c(2:length(l)), function(j) { i <- j - 1; wilcox.test(ffmeans[[i]],ffmeans[[j]])$p.value })
varspvs <- sapply(c(2:length(l)), function(j) { i <- j - 1; wilcox.test(ffvars[[i]],ffvars[[j]])$p.value })
meansdiffs <- sapply(c(2:length(l)), function(j) { i <- j - 1; mean(ffmeans[[j]]) - mean(ffmeans[[i]]) })
varsdiffs <- sapply(c(2:length(l)), function(j) { i <- j - 1; mean(ffvars[[j]]) - mean(ffvars[[i]]) })
boxplot(l,names=ffversionorder)
boxplot(ffkwh,names=ffversionorder)
boxplot(ffwh,names=ffversionorder)
plot(kwhpvs)
lines(kwhpvs*0 + 0.05)

# generate
min36 <- c(1:length(ffversionorder))[ffversionorder == "3.6a2pre"]

ffavgwattage <- function(leg=FALSE,legx=5,legy=27) {
  lx <- c(1:(length(ffmeans)))
  xx <- c(lx,rev(lx))
  yy <- c(sapply(l,max),rev(sapply(l,min)))
  plot(c(),xlim=c(1,length(l)),ylim=c(min(sapply(l,min)), max(sapply(l,max))),xaxt="n",ylab="Mean Watts",xlab="Firefox Version",main="Wattage of Browsing Tests per version of Firefox 3.6 (and a sampling of earlier versions)")
  polygon(xx,yy,col="aquamarine",border="darkgreen",lty=2)  
  boxplot( l, col="white", names=ffversionorder,add=1)
  #abline(mean(sapply(l,mean)),0,lty=3)
  lmfit <- line(lx,sapply(ffmeans,mean))
  abline( coef(lmfit), lty=3,lw=1)
  lines( lx, sapply(l, mean),col="red",lw=4,lty=5)
  if (leg) {
    namess <- c("Mean of Mean Watts per Version","Range of Min and Max Mean Watts per Version","Line of best fit: Mean Watts Versus Version")
    cols <-   c("red"               ,"aquamarine"                    ,"black")
    ltys <-   c(5                   ,1                               ,3)
    lws  <-   c(4                   ,10                              ,1)
    legend("topright", namess, col=cols, lwd=lws, lty=ltys)
  }
}
ffavgwattage()

pdf("firefox-tests.pdf",width=15,height=5)
ffavgwattage(leg=TRUE)
dev.off()


ffmeanmeans <- sapply(ffversionorder,function(x){mean(ov[ov$ffversion == x,c("mean")])})


rtavgwattage <- function(ov,ltorrent=FALSE,leg=FALSE,legx=5,legy=27) {
  surinames <- unique(sort(ov$sURI))
  ov$rtorrents <- getRtorrentVersion(ov)
  ov$ltorrents <- getLtorrentVersion(ov)
  ourNames <- c()
  xlabel <- ""
  if (ltorrent) {
    ov$name <- ov$ltorrents
    xlabel <- "LibTorrent Version"
  } else {
    ov$name <- ov$rtorrents
    xlabel <- "RTorrent Version"
  }
  ourNames <- quicksort(as.character(unique(ov$name)),fflt)
  means     <- lapply( ourNames, function(x) { ov[ov$name == x,c("mean")] } )
  lx <- c(1:(length(means)))
  xx <- c(lx,rev(lx))
  yy <- c(sapply(means,max),rev(sapply(means,min)))
  plot(c(),xlim=c(1,length(means)),ylim=c(min(sapply(means,min)), max(sapply(means,max))),xaxt="n",ylab="Mean Watts",xlab=xlabel,main="Wattage of BitTorrent Tests per version of Rtorrent")
  polygon(xx,yy,col="aquamarine",border="darkgreen",lty=2)  
  boxplot( means, col="white", names=ourNames,add=1)
  #abline(mean(sapply(l,mean)),0,lty=3)
  lmfit <- line(lx,sapply(means,mean))
  abline( coef(lmfit), lty=3,lw=1)
  lines( lx, sapply(means, mean),col="red",lw=4,lty=5)
  if (leg) {
    namess <- c("Mean of Mean Watts per Version","Range of Min and Max Mean Watts per Version","Line of best fit: Mean Watts Versus Version")
    cols <-   c("red"               ,"aquamarine"                    ,"black")
    ltys <-   c(5                   ,1                               ,3)
    lws  <-   c(4                   ,10                              ,1)
    legend("bottomleft", namess, col=cols, lwd=lws, lty=ltys)
  }
  ret <- list()
  ret$xx <- xx
  ret$yy <- yy
  ret$means <- means
}
rtavgwattage(ov,ltorrent=TRUE,leg=TRUE)
rtavgwattage(ov,ltorrent=FALSE,leg=TRUE)





vuzeavgwattage <- function(ov,leg=FALSE,legx=5,legy=27) {
  surinames <- unique(sort(ov$sURI))
  xlabel <- "Vuze Versions"
  ov$name <- getVuzeVersion( ov )
  ourNames <- quicksort(as.character(unique(ov$name)),fflt)
  means     <- lapply( ourNames, function(x) { ov[ov$name == x,c("mean")] } )
  lx <- c(1:(length(means)))
  xx <- c(lx,rev(lx))
  yy <- c(sapply(means,max),rev(sapply(means,min)))
  plot(c(),xlim=c(1,length(means)),ylim=c(min(sapply(means,min)), max(sapply(means,max))),xaxt="n",ylab="Mean Watts",xlab=xlabel,main="Wattage of BitTorrent Tests per version of Vuze")
  polygon(xx,yy,col="aquamarine",border="darkgreen",lty=2)  
  boxplot( means, col="white", names=ourNames,add=1)
  #abline(mean(sapply(l,mean)),0,lty=3)
  lmfit <- line(lx,sapply(means,mean))
  abline( coef(lmfit), lty=3,lw=1)
  
  #lmfit <- line(lx,sapply(means,median))
  #abline( coef(lmfit), lty=3,lw=1)

  #lines( lx, sapply(means, median),col="blue",lw=4,lty=5)
  lines( lx, sapply(means, mean),col="red",lw=4,lty=5)
  if (leg) {
    namess <- c("Mean of Mean Watts per Version","Range of Min and Max Mean Watts per Version","Line of best fit: Mean Watts Versus Version")
    cols <-   c("red"               ,"aquamarine"                    ,"black")
    ltys <-   c(5                   ,1                               ,3)
    lws  <-   c(4                   ,10                              ,1)
    legend("bottomleft", namess, col=cols, lwd=lws, lty=ltys)
  }
  ret <- list()
  ret$xx <- xx
  ret$yy <- yy
  ret$means <- means
  ret
}
ret <- vuzeavgwattage(ov,leg=TRUE)
