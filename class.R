measures <- read.csv("real-ckjm.csv",sep=" ")
versions <- sort(unique(measures$Version))
totalClasses <- sapply(versions,function(x){length(unique(measures[measures$Version==x,]$Class))})

sumWMC <- sapply(versions,function(x){sum(measures[measures$Version==x,"WMC"])})
sums <- c()
sums$names <- versions
columnNames <- names(measures)
columnNames <- columnNames[-1*c(1,2)]
for (col in columnNames) {
  sums[[col]] <- sapply(versions,function(x){sum(measures[measures$Version==x,col])})
}
sumsdx <- c()
sumsdx$names <- versions[-1]
for (col in columnNames) {

  sumsdx[[col]] <- sapply(c(2:length(versions)),
                          function(i) {
                            sums[[col]][i] - sums[[col]][i-1]
                          })

}





# stuff we typed.

savehistory(file = ".what") 	help(history)
history(max.show=9999) 	help(history)
history() 	history(max.show=9999)
history 	history(max.show=9999)
cor(as.matrix(sumsdx)) 	cor(sumsdx)
lmfit <- lm( wattsdx ~ totalClassesdx + AMC + CBM + NOC , data=data.frame(sumsdx)); summary(lmfit)
sumsdx$totalClassesdx <- totalClasses[2:45] - totalClasses[1:44]
lmfit <- lm( wattsdx ~ totalClasses + AMC + CBM + NOC , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( wattsdx ~ AMC + CBM + NOC , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + CBM + NOC , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + CBM +  + MOA + LCO + NOC , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + CBM + IC + MFA + MOA + LCO + NOC , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + WMC+ MFA +  LCO + NOC + DIT + CBO + RFC + LCOM +Ca + Ce , data=data.frame(sumsdx)); summary(lmfit)
sumsdx$totalClasses <- totalClasses[2:45]
lmfit <- ols( watts ~ AMC + WMC+ MFA +  LCO + NOC + DIT + CBO + RFC + LCOM +Ca + Ce , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + WMC+ MFA +  LCO + NOC + DIT + CBO + RFC + LCOM +Ca + Ce , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- glm( watts ~ AMC + WMC+ MFA +  LCO + NOC + DIT + CBO + RFC + LCOM +Ca + Ce , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- glm( watts ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( wattsdx ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( watts ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sums)); summary(lmfit)
lmfit <- lm( wattsdx ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sums)); summary(lmfit)
lmfit <- lm( wattsdx ~ AMC + WMC+ MFA +  LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
AIC(lmfit) 	vif(lmfit)
lmfit <- lm( wattsdx ~ MFA +  LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( wattsdx ~ MFA + IC + CBM + LCO + NOC  , data=data.frame(sumsdx)); summary(lmfit)
lmfit <- lm( wattsdx ~ MFA + IC + CBM + LCO + NOC  , data=data.frame(ourmat)); summary(lmfit)
lmfit <- lm( watts ~ MFA + IC + CBM + LCO + NOC  , data=data.frame(ourmat)); summary(lmfit)
lmfit <- lm( wattsdx ~ MFA + IC + CBM + LCO + NOC  , data=data.frame(ourmat)); summary(lmfit)
lmfit <- lm( wattsdx ~ . , data=data.frame(ourmat)); summary(lmfit)
lmfit <- lm( wattsdx ~ NOC + LCO , data=data.frame(ourmat)); summary(lmfit)
names(sumsdx) 	names(sumdx)
names(ourmat)
lmfit <- lm( wattsdx ~ WMC + NOC, data=data.frame(ourmat)); summary(lmfit)
summary(lmfit)
lmfit <- lm( wattsdx ~ WMC + NOC, data=data.frame(ourmat))
summary(lmfit)
lmfit <- lm( wattsdx ~ WMC + DIT + NOC + CBO + RFC + LCOM +Ca + Ce + LCOM3, data=data.frame(ourmat))
lm( wattsdx ~ WMC + DIT + NOC + CBO + RFC + LCOM +Ca + Ce + LCOM3, data=data.frame(ourmat))
lm( wattsdx ~ WMC + DIT + NOC + CBO + RFC + LCOM +Ca + Ce + LCOM3, data=ourmat)
lm.fit( sumdx$wattsdx ~ sumdx$WMC )
lm.fit( wattsdx ~ WMC + DIT + NOC + CBO + RFC , data=t(ourmat))
lm.fit( wattsdx ~ WMC + DIT + NOC + CBO + RFC , data=ourmat)
lm.fit( wattsdx ~ WMC + DIT + NOC + CBO + RFC , data=as.list(ourmat))
lm.fit( wattsdx ~ WMC + DIT + NOC + CBO + RFC + LCOM +Ca + Ce + LCOM3, data=as.list(ourmat))
lm.fit( wattsdx ~ WMC + DIT + NOC + CBO + RFC + LCOM +Ca + Ce + LCOM3, data=ourmat)
cor(ourmat,method="spearman") 	corrplot(cor(ourmat,method="spearman"))
corrplot(cor(ourmat,method="Spearman")) 	corrplot(cor(ourmat))
library(corrplot) 	cor(ourmat)
ourmat <-matrix(unlist(sumsdx), nrow=44, dimnames=list(sumsdx$names,names(sumsdx)))
matrix(unlist(sumsdx), nrow=44, dimnames=list(sumsdx$names,names(sumsdx)))
matrix(unlist(sumsdx), nrow=44, dimnames=list(names(sumsdx),sumsdx$names))
names(sumsdx)
matrix(unlist(sumsdx), nrow=44, dimnames=list(names(sumsdx),sumsdx$name))
matrix(unlist(sumsdx), nrow=44, dimnames=list(names(sumsdx),c(1:44)))
matrix(unlist(sumsdx), nrow=44, dimnames=list(names(sumsdx),names(sumsdx)))
matrix(unlist(sumsdx), nrow=44) 	)
matrix(unlist(sumsdx), nrow=45
matrix(unlist(sumsdx), ncol=length(names(sumsdx)), dimnames=list(names(sumsdx),names(sumsdx)))
matrix(unlist(sumsdx), nrow=21, dimnames=list(names(sumsdx),names(sumsdx)))
matrix(unlist(sumsdx), nrow=45, dimnames=list(names(sumsdx),names(sumsdx)))
matrix(unlist(sumsdx), nrow=45, dimnames=names(sumsdx))
help(matrix) 	matrix(unlist(sumsdx), nrow=45)
sumsdx$names 	unlist(sumsdx)
cbind(sumsdx) 	 unlist(sumsdx)
flatten 	as.matrix(flatten(sumsdx))
as.matrix(sumsdx) 	is.list(sumsdx)
isa(sumsdx) 	cor(as.vector(sumsdx))
as.vector(sumsdx) 	sumsdx
sapply(c(2:21),function(x){ length(sumsdx[[x]]) })
cor(as.matrix(lapply(c(2:21),function(x){ sumsdx[[x]] })))
cor(lapply(c(2:21),function(x){ sumsdx[[x]] }))
lapply(c(2:21),function(x){ sumsdx[[x]] })
sapply(c(2:21),function(x){ length(sumsdx[[x]]) })
sapply(c(2:21),function(x){ length(sumsdx[,x]) })
as.matrix(sumsdx[,c(2:21)]) 	as.matrix(sumsdx[c(2:21),])
as.matrix(sumsdx[c(2:21)]) 	cor(as.matrix(sumsdx[c(2:21)]))
as.matrix(sumsdx[c(2:21)]) 	sumsdx[c(2:21)]
sumsdx[c(2:21),] 	sumsdx[,c(2:21)]
cor(sumsdx[,c(2:21)]) 	length(names(sumsdx))
names(sumsdx) 	cor(sumsdx)
cor(sumsdx$watts,sumsdx$WMC)
cor(as.matrix(sumsdx[,c(2:length(names(sumsdx)))]))
cor(sumsdx[,c(2:length(names(sumsdx)))])
cor(sumsdx[,2:length(names(sumsdx))]) 	cor(sumsdx)
sumsdx$wattsdx <- meanwattsdx 	sumsdx$watts <- meanwatts[2:45]
sumsdx$watts <- meanwattsdx 	meanwattsdx
meanwatts
meanwattsdx <- sapply(c(2:length(meanwatts)), function(i) { meanwatts[i] - meanwatts[i - 1]})
meanwatts <- sapply(ret$means,mean) 	sapply(ret$means,mean)
length(sumsdx$WMC) 	length(sumsdx)
ret 	sumsdx
plot(sums[,columnNames]) 	plot(sums)
sums 	columnNames
sumWMC 	wumWM
names(measures) 	versions
summary(measures$WMC) 	summary(measures$X)
names(measures) 	summary(measures$Class)
unique(measures$Version) 	names(measures)
NN <- 24; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 400000; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 40000; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 40; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 20; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 10; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
NN <- 100; sapply(c(1:(length(ret$means)-1)),function(i) { 0.05 > t.test(sample(ret$means[[i]],NN,TRUE),sample(ret$means[[i+1]],NN,TRUE))$p.value})
