#!/bin/bash
rm /tmp/k2.csv
rm /tmp/powerplot.pdf
zcat $1 | perl ~/projects/green-logger/green-logger-to-csv.pl > /tmp/k2.csv
run-r ~/projects/green-logger/green.R
xpdf /tmp/powerplot.pdf
