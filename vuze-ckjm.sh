VUZES=`(cd ~/green-results; bash watchvuze | tail -n +2 | awk '{print $2}' | sort)`
mkdir cache
(
cat ckjm.csv.head 
for file in $VUZES
do
	JAR=cache/vuze-$file
	if [ ! -e $JAR ];
	then
		URL=http://softwareprocess.es/vuze/vuze-$file
		echo "Getting $URL"
		(cd cache ; wget $URL)
	fi
	VERSION=`echo $JAR | sed -e 's/vuze-\([0-9]\+\)\.jar/\1/' `
	java -jar ckjm_ext.jar $JAR | egrep '^[a-zA-Z]' | awk "{print \"$VERSION \" \$0}"
done
) | tee vuze-ckjm.csv
